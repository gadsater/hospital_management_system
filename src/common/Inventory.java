package common;

import java.util.*;

public class Inventory {
	

	public static Hashtable<String, Integer> inventory = new Hashtable<String, Integer>();

	public Inventory()
	{
		inventory.put("item1", 10);
		inventory.put("item2", 20);
	}
	
	
	public void addInventoryRecords(String item, int quantity)
	{
		if(quantity<0)
		{
			return;
		}
		else
			this.inventory.put(item, quantity);
	}
	
	
	
	public void deleteInventoryRecords(String item, int quantity)
	{
		if(quantity>this.inventory.get(item))
		{
			return;
		}
		else
		{
			int x=this.inventory.get(item);
			this.inventory.remove(item);
			this.inventory.put(item, (x-quantity));
		}
	}
	
	public String viewInventoryRecords()
	{
		return this.inventory.toString();
	}
	
	public String toString()
	{
		StringBuilder items = new StringBuilder();
		Set<String> keys = this.inventory.keySet();
		for(String key:keys)
		{
			items.append(key);
			items.append(" ");
			items.append("quantity:");
			items.append(this.inventory.get(key));
			items.append("\n");
			String item=items.toString();
		}
		
		if(items.length()> 0)
		{
			return items.toString();
		}
		else
		{
			return "No items in inventory";
		}
	}
	
	
}
