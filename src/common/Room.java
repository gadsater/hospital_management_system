package common;

import java.util.Hashtable;

public class Room {
	
	public static Hashtable<String, Boolean> room = new Hashtable<String, Boolean>();
	
	public Room() {
		room.put("A401", true);
		room.put("A402", true);
		room.put("A403", true);
		room.put("A404", true);
		room.put("A405", true);
		room.put("A406", true);
		room.put("A407", true);
		room.put("A408", true);
		room.put("A409", true);
		room.put("A410", true);
		room.put("A411", true);
		room.put("A412", true);
	}
	
	public void bookRoom(String roomname)
	{
		if(roomStatus(roomname)==false)
		{
			return;
		}
		else
		{
			room.remove(roomname);
			room.put(roomname, false);
		}
	}
	
	public void makeRoomAvailable(String roomname)
	{
		if(roomStatus(roomname)==true)
		{
			return;
		}
		else
		{
			room.remove(roomname);
			room.put(roomname, true);
		}
	}
	
	public Boolean roomStatus(String roomname)
	{
		return room.get(roomname);
	}
}
