package common;
import java.io.*;
import java.util.*;
import java.time.LocalTime;


public class Doctor extends Employee{

	Doctor(String username, String name) {
		this.setName(name);
		this.setUserName(username);
	}

	public void TreatPatient(Patient p) {

		Scanner in = new Scanner(System.in);
		System.out.println("Enter the diagnosis");

		String diagnosis = in.nextLine();

		System.out.println("Enter the prescription");

		//Prescription info
		String pres = in.nextLine();
		
		//Time of Record
		LocalTime lt = LocalTime.now();

		//New Instance of patient record
		PatientRecord pr = new PatientRecord(lt , diagnosis, pres, this.getName());

		//Add record to patient's history
		p.AddRecord(pr);
		
		in.close();
	}
}
